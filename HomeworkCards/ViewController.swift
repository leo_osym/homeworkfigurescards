//
//  ViewController.swift
//  HomeworkCards
//
//  Created by user153878 on 4/9/19.
//  Copyright © 2019 leo_osym. All rights reserved.
//

import UIKit
import MobileCoreServices

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var cards: [Card]!
    @IBOutlet weak var cardBack: Card!
    
    @IBAction func changeImage(_ sender: UIBarButtonItem) {
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.mediaTypes = [kUTTypeImage as String]
        picker.allowsEditing = true
        picker.delegate = self
        present(picker,animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.presentingViewController?.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] ?? info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            cardBack.image = image as? UIImage
        }
        picker.presentingViewController?.dismiss(animated: true)
    }
    
    lazy var animator = UIDynamicAnimator(referenceView: view)
    lazy var cardBehavior = CardBehavior(in: animator)

    override func viewDidLoad() {
        super.viewDidLoad()
        // init main set of views: their figures and gestures
        initButtons()
        // init details view
        cardBack.sides = 0
        cardBack.isHidden = true
        cardBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onCardBackTapped)))
        
        let swipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(onCardBackSwiped))
        swipeRecognizer.direction = [.left,.right]
        cardBack.addGestureRecognizer(swipeRecognizer)
        cardBack.isFaceUp = false
        // initial
        cardBack.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
    }
    
    // set top margin to exclude navbar from collisionBehaviour boundaries
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let topMargin = (navigationController?.navigationBar.frame.maxY)!
        cardBehavior.setTopMargin(topMargin: topMargin)
        for card in cards {
            cardBehavior.addItem(card)
        }
    }
    
    private func initButtons(){
        cards[0].sides = 0
        cards[1].sides = 1
        cards[2].sides = 2
        cards[3].sides = 3
        cards[4].sides = 5
        cards[5].sides = 8
        
        for card in cards {
            card.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onCardTapped(_:))))
        }
    }
    
    @objc func onCardTapped(_ tap: UITapGestureRecognizer) {
        callCardBack(tap: tap)
    }
    
    private func callCardBack(tap: UITapGestureRecognizer){
        let tappedCard = tap.view as? Card
        
        if let numberOfSides = tappedCard?.sides {
            cardBack.sides = numberOfSides
        }
        hideBackCard(false)
    }
    
    @objc func onCardBackTapped(tap: UITapGestureRecognizer) {
        switch tap.state {
        case .ended:
            let card = tap.view as? Card
            // flip card
            UIView.transition(with: cardBack,
                              duration: 0.6,
                              options: [.transitionFlipFromLeft],
                              animations: {[weak self] in
                                card?.isFaceUp = !card!.isFaceUp
                                self?.cardBack.flipCounter += 1
                             },
                              // if card is already seen - scale it to small with animation
                              completion: { [weak self] _ in
                                if(self?.cardBack.flipCounter == 2){
                                    self?.cardBack.flipCounter = 0
                                    UIViewPropertyAnimator.runningPropertyAnimator(
                                        withDuration: 0.6,
                                        delay: 0,
                                        options: [],
                                        animations: {[weak self] in
                                            self?.cardBack.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
                                            //and hide it after animation:
                                    }, completion: { _ in
                                        self?.hideBackCard(true)
                                    })
                                }
                                })
        default:
            break
        }
        
        
    }
    
    @objc func onCardBackSwiped(swipe: UISwipeGestureRecognizer) {
        hideBackCard(true)
    }
    
    func hideBackCard(_ hide: Bool){
        if(hide == true){
            cardBack.isHidden = true
            for card in cards {
                card.isHidden = false
                cardBehavior.addItem(card)
            }
        } else {
            cardBack.isHidden = false
            for card in cards {
                card.isHidden = true
                cardBehavior.removeItem(card)
            }
            // if taped - scale cardBack to original size with animation
            UIViewPropertyAnimator.runningPropertyAnimator(
                withDuration: 0.6,
                delay: 0,
                options: [],
                animations: {[weak self] in
                    self?.cardBack.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
            })
        }
        // always show card's back
        cardBack.isFaceUp = false
    }
    
}

extension CGFloat {
    var arc4random: CGFloat {
        return self * (CGFloat(arc4random_uniform(UInt32.max))/CGFloat(UInt32.max))
    }
}

