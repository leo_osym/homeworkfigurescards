//
//  Card.swift
//  HomeworkCards
//
//  Created by user153878 on 4/9/19.
//  Copyright © 2019 leo_osym. All rights reserved.
//

import Foundation
import UIKit

class Card : UIView {
    
    var image = UIImage(named: "stanford-tree")
    
    var sides: Int? {
         didSet {setNeedsDisplay(); setNeedsLayout()}
    }
    var isFaceUp = true { didSet{setNeedsDisplay(); setNeedsLayout()}}
    var flipCounter = 0
    
    init(frame: CGRect, sides: Int) {
        super.init(frame: frame)
        self.sides = sides
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        if isFaceUp {
            if sides != nil && sides! > 2 {
                drawFigure(context: context, rect: rect, sides: sides!)
            }
            drawCustomText(context: context, rect: rect, text: String(sides!))
        } else {
            if let backImage = image?.rotate(radians: CGFloat.pi) {
                context.draw(backImage.cgImage!, in: rect)
            }
        }
        setNeedsDisplay()
        setNeedsLayout()
    }
    
    /// draw text inside figures
    private func drawCustomText(context: CGContext, rect: CGRect, text: String){
        let tex: NSString = text as NSString
        let fontSize = 30.0
        let font = UIFont(name: "Helvetica Bold", size: CGFloat(fontSize))!
        let textRect = CGRect(x: rect.maxX/2 - CGFloat(fontSize)/3, y: rect.maxY/2 - CGFloat(fontSize)/3, width: CGFloat(fontSize), height: CGFloat(fontSize))
        
        let attributeDict: [NSAttributedString.Key : Any] = [
            .font: font,
            .foregroundColor: UIColor.red
        ]
        
        tex.draw(in: textRect, withAttributes: attributeDict)
    }
    
    ///draw regular polygons using by number of sides
    private func drawFigure(context: CGContext, rect: CGRect, sides: Int){
        
        // set margins
        let newMinX = rect.minX+5
        let newMaxX = rect.maxX-5
        
        //radius of the circumscribed circle
        let outerRadius = Double(newMaxX-newMinX)/2
        //length of each side
        let sideLength = 2.0 * outerRadius * sin(Double.pi / Double(sides))
        //inner angle betweeen sides
        let angle = -360.0/Double(sides)
        //radius of the inscribed circle
        let innerRadius = sideLength/(2*tan(Double.pi/Double(sides)))
        //startpoint
        //var point = (x: Double(rect.maxX/2) + sideLength/2, y: (Double(newMaxY/2) + outerRadius))
        var point = (x: Double(rect.maxX/2) + sideLength/2, y: Double(rect.maxY/2) + innerRadius)
        // start from:
        let path = UIBezierPath()
        path.move(to: CGPoint(x: point.x, y: point.y))
        // compute position: new point using rotation angle
        for i in 1...sides-1{
            let sinFi = sin(Double(i) * angle * Double.pi / 180.0)
            let cosFi = cos(Double(i) * angle * Double.pi / 180.0)
            point.x = point.x + sideLength * cosFi
            point.y = point.y + sideLength * sinFi
            path.addLine(to: CGPoint(x: point.x, y: point.y))
        }
        path.close()

        UIColor.red.setStroke()
        UIColor.green.setFill()
        path.lineWidth = 5
        path.stroke()
        path.fill()
    }
}

extension UIImage {
    func rotate(radians: CGFloat) -> UIImage {
        let rotatedSize = CGRect(origin: .zero, size: size)
            .applying(CGAffineTransform(rotationAngle: CGFloat(radians)))
            .integral.size
        UIGraphicsBeginImageContext(rotatedSize)
        if let context = UIGraphicsGetCurrentContext() {
            let origin = CGPoint(x: rotatedSize.width / 2.0,
                                 y: rotatedSize.height / 2.0)
            context.translateBy(x: origin.x, y: origin.y)
            context.rotate(by: radians)
            draw(in: CGRect(x: -origin.x, y: -origin.y,
                            width: size.width, height: size.height))
            let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return rotatedImage ?? self
        }
        
        return self
    }
}
